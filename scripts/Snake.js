//razred snake
function Snake(){
	this.dolzina = 1;
	this.x = 0;
	this.y = 0;
	this.zivljenje = 3;
	this.telo =  new Array();
	for (var i=0;i<this.dolzina;i++) {
		this.telo.push(new Telo(i+1,0));
	}
}
Snake.prototype.getX = function(){
	return this.x;
};
Snake.prototype.getY = function(){
	return this.y;
};
Snake.prototype.setX = function(x){
	this.x = x;
};
Snake.prototype.setY = function(y){
	this.y = y;
};
Snake.prototype.checkCollision = function(item){
	if(this.x == item.getX() && this.y == item.getY()){
		return true;
	}
	return false;
};
Snake.prototype.addTelo = function(){
	telo.push(new Telo(1,1));
};
Snake.prototype.move = function(velocityX, velocityY){
	var prevX = this.x;
	var prevY = this.y;
	if(velocityX == 1){
		this.x++;
	} else if(velocityX == -1){
		this.x--;
	} else if(velocityY == 1){
		this.y++;
	} else if(velocityY == -1){
		this.y--;
	}

	for(var i=0;i<this.telo.length;i++){
		prevX = this.telo[i].getX();
		prevY = this.telo[i].getY();
		this.telo[i].setCords(prevX,prevY);
	}
}