<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Rattler race</title>
    <script type="text/javascript" src="js/jquery-1.8.2.js"></script>
    <script src="scripts/soundmanager2-jsmin.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Press+Start+2P' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="js/three.js"></script>
    <script type="text/javascript" src="js/MTLLoader.js"></script>
    <script type="text/javascript" src="js/OBJLoader.js"></script>
    <script type="text/javascript" src="js/OBJMTLLoader.js"></script>
    <script type="text/javascript" src="js/PointerLockControls.js"></script>
    <script type="text/javascript" src="js/OrbitControls.js"></script>
    <script type="text/javascript" src="js/THREEx.KeyboardState.js"></script>
    <script type="text/javascript" src="js/Telo.js"></script>
    <script type="text/javascript" src="js/Snake.js"></script>
    <script type="text/javascript" src="js/Item.js"></script>
    <style>
        html, body {
            padding: 0;
            margin: 0;
            font-family: 'Press Start 2P', cursive;
        }

        #content {
            width: 100%;
            height: 230px;
            float: left;
            background: #2aaa00;
            /* Old browsers */
            background: -moz-linear-gradient(top, #2aaa00 0%, #61c419 50%, #2aaa00 100%);
            /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #2aaa00), color-stop(50%, #61c419), color-stop(100%, #2aaa00));
            /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, #2aaa00 0%, #61c419 50%, #2aaa00 100%);
            /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, #2aaa00 0%, #61c419 50%, #2aaa00 100%);
            /* Opera 11.10+ */
            background: -ms-linear-gradient(top, #2aaa00 0%, #61c419 50%, #2aaa00 100%);
            /* IE10+ */
            background: linear-gradient(to bottom, #2aaa00 0%, #61c419 50%, #2aaa00 100%);
            /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#2aaa00', endColorstr='#2aaa00', GradientType=0);
            /* IE6-9 */
        }

        #game {
        }

        #level {
            margin: -70px auto auto 130px;
            color: white;
            font-size: 30px;
        }

        #zivljenja {
            margin: -20px auto -10px 190px;
        }

        #cas {
            margin-left: 510px;
            margin-top: -36px;
            color: white;
            font-size: 30px;
            text-align: center;
        }

        #tocke {
            margin: 0px auto auto 130px;
            color: white;
            font-size: 30px;
        }

        #nastavitve {
            border: 6px solid white;
            border-radius: 5px;
            background-color: #ffbd0c;
            height: 200px;
            box-shadow: 0 0 0 10px orange;

        }

        canvas {
            width: 100%;
            z-index: -100;
            position: relative;
        }


        /* REZULTATI */
        #top-ten {
            border: 6px solid white;
            border-radius: 5px;
            background-color: #ffbd0c;
            height: 350px;
            box-shadow: 0 0 0 10px orange;
            text-align: center;
            font-family: 'Press Start 2P', cursive;
            color: white;
        }

        #naslov{
            margin-top: -20px;
            color: white;
        }

        #top-ten table {
            display: table;
            margin: 0 auto;
            border: 3px solid white;
            border-collapse: collapse;
        }

        #tabela{
            padding-top: 5px;

        }

        #top-ten table tr td {
            border: 3px solid white;
            border-spacing: 0px;
            padding: 5px;
        }

        #zapri-tt{
            margin-right: -95%;
            margin-top: 10px;
        }
		
        #zapri-tt:hover, #nova-igra:hover{
            text-shadow:2px 2px #000000;
            cursor: pointer;
        }

        #nova-igra{
            margin-top: 5px;
        }

        /* REZULTATI */
    </style>
</head>
<body>
<video id="monitor" autoplay style="display:none; width: 307px; height: 230px;"></video>

<div id="canvasLayers" style="position: absolute; width: 307px; height: 230px;top:0; right: 0;">
    <canvas id="videoCanvas" width="320" height="240"
            style="z-index: 1; position: absolute; left:0px; top:0px; opacity:1;"></canvas>
    <canvas id="layer2" width="320" height="240"
            style="z-index: 2; position: absolute; left:0px; top:0px; opacity:1;"></canvas>
</div>
<canvas id="blendCanvas"
        style="display: none; position: relative; left: 320px; top: 240px; width: 307px; height: 230px;"></canvas>
<div id="messageArea" style="position: relative; left: 0px; top: 270px;"></div>
<script>
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    window.URL = window.URL || window.webkitURL;

    var camvideo = document.getElementById('monitor');

    if (!navigator.getUserMedia) {
        document.getElementById('messageError').innerHTML =
            'Sorry. <code>navigator.getUserMedia()</code> is not available.';
    }
    navigator.getUserMedia({video: true}, gotStream, noStream);

    function gotStream(stream) {
        if (window.URL) {
            camvideo.src = window.URL.createObjectURL(stream);
        }
        else // Opera
        {
            camvideo.src = stream;
        }

        camvideo.onerror = function (e) {
            stream.stop();
        };

        stream.onended = noStream;
    }

    function noStream(e) {
        var msg = 'No camera available.';
        if (e.code == 1) {
            msg = 'User denied access to use camera.';
        }
        document.getElementById('errorMessage').textContent = msg;
    }
</script>
<div id='content'>
    <div id='header' style='width:720px; margin:0 auto;'>
        <img src='logo.png' style='display:block; '/>
        <img src='scoreboard.png' style='display:block; margin-top:-20px;'/>
        <img src='logo-snake.png' style='display:block; margin: -230px auto auto 285px'/>

        <div id='scoreboard'>
            <div id='level'>
                1
            </div>
            <div id='zivljenja'>
                <img src="srce.png" width='45' height='45' />
                <div style="color:red;float:left; position:relative; top:15px; left:32px;"></div>
            </div>
            <div id='tocke'>
                0
            </div>
            <div id='cas'>
            </div>
        </div>
        <img id='nastavitveImg' src='nastavitve.png' style='display:block; margin-left:350px; margin-top:-10px;'/>

        <div id='nastavitve' style='display:none; color:white;'>
            <div style='margin-top:15px; text-align:center;'>Nastavitve</div>
            <br/>
            <div style='margin-left:5px;'>Level <input type="number" id="levelSlider" value='1' min="1" max="30"></div>
            <div style='margin-left:5px;'>Tezavnost </span><input type="number" id='tezavnostSlider' value='0' min="0" max="3"></div>
            <div style='margin-left:5px;'>Stevilo nasprotnikovih kac </span><input type="number" id='kaceSlider' value='0' min="0" max="3"></div>
            <div style='margin-left:5px;'>Stevilo zog </span><input type="number" id='zogeSlider' min="0" value='0' max="3"></div><br/>
            <img height='32px' width='32px' src='mute.png' id='zvokSlika'/>
            <img src='slike/level1.jpg' id='levelSlika' style='float:right; margin-top:-150px; margin-right:20px;' height='150px' width='150px'/>
            <p id='shrani' style='text-align:center; margin-top:-25px; cursor:pointer;'>Shrani nastavitve</p><br/>
        </div>
    </div>
    <div id='game'>
    </div>
</div>
<script>
$("#zvokSlika").click(function () {
        if($(this).attr('src')=="mute.png"){
            $(this).attr('src','unmute.png');
            localStorage.zvok='off';
            soundManager.mute();
        }else{
            $(this).attr('src','mute.png');
            localStorage.zvok='on';
            soundManager.unmute();
        }
    }
);

$("#levelSlider").change(function () {
    var vrednost = $('#levelSlider').val();
    $("#levelSlika").attr("src","slike/level"+vrednost+".jpg");
})
$("#tezavnostSlider").change(function () {
    var vrednost = $('#tezavnostSlider').val();
    localStorage.tezavnost=vrednost;
})
soundManager.setup({
        url: '',
        debugMode: false,
        onfinish: function () {

        },
        onready: function () {
            var sik = soundManager.createSound({
                    id: 'sikanje',
                    url: './sounds/sikanje.mp3'
                }
            );
            var dingdong = soundManager.createSound({
                    id: 'ding',
                    url: './sounds/ding.mp3'
                }
            );
            var jabuk = soundManager.createSound({
                    id: 'jabuk',
                    url: './sounds/jabolk.mp3'
                }
            );
            var igra = soundManager.createSound({
                    id: 'igra',
                    url: './sounds/igra.mp3'
                }
            );
            var konec = soundManager.createSound({
                    id: 'konec',
                    url: './sounds/konec.mp3'
                }
            );
            soundManager.setVolume('igra',30);
            soundManager.setVolume('jabuk',200);
            soundManager.play('igra',{loops: 10});

        }
    }
);
var scene, camera, renderer, topCamera;
var snakeObject;
var cloneObject;
var WIDTH = window.innerWidth;
var keyboard = new THREEx.KeyboardState();
var HEIGHT = window.innerHeight - 230;
var snake;
var clock = new THREE.Clock();
var prevX, prevY;
//var zoga, zogaBox;
var snakeHead;
var collidableMeshList = [];
var currentLevel = 1;
var jabolka = [];
var zoge = [];
var zogeBox = [];
var mesh;
var stena;
var id_render;
var textureRdeca, textureZlata, textureZid, textureVrata;
var pauseInverval = 80;
var steviloTeles = 20;
var hitrostKace = 400;
var rotateAgnle;
var hitrostX = new Array(10,10,10);
var hitrostY = new Array(10,10,10);
var telesa = 0;
var tezavnost = 0;
var tocke = 0;
var kace=0;
var st_zog=0;
var zivljenja = 3;
var levaVrata, desnaVrata;
var odprtaVrata = false;
var jabolkoMesh;
var snakeOther1 = new Array();
var buttons;
var lastImageData;

// assign global variables to HTML elements
var video = document.getElementById('monitor');
var videoCanvas = document.getElementById('videoCanvas');
var videoContext = videoCanvas.getContext('2d');

var layer2Canvas = document.getElementById('layer2');
var layer2Context = layer2Canvas.getContext('2d');

var blendCanvas = document.getElementById("blendCanvas");
var blendContext = blendCanvas.getContext('2d');

var messageArea = document.getElementById("messageArea");

getZivljenja();
getLevel();
getTocke();
getOstaleKace();
getZoge();
getTezavnost();
initTezavnost();
init();

function getOstaleKace(){
	if (localStorage.kace != null) {
        kace = localStorage.kace;
    } else {
        localStorage.kace = 0;
    }
}

function getZoge(){
	if (localStorage.zoge != null) {
        st_zog = localStorage.zoge;
    } else {
        localStorage.zoge = 0;
    }
}

function getTezavnost(){
    if (localStorage.tezavnost != null) {
        tezavnost = localStorage.tezavnost;
    } else {
        localStorage.tezavnost = 0;
    }
}

function initTezavnost() {
    switch (tezavnost) {
        case '0':
            steviloTeles = 10;
            hitrostKace = 400;
            pauseInverval = 80;
            break;
        case '1':
            steviloTeles = 20;
            hitrostKace = 700;
            pauseInverval = 50;
            break;
        case '2':
            steviloTeles = 30;
            hitrostKace = 900;
            pauseInverval = 40;
            break;
        case '3':
            steviloTeles = 30;
            hitrostKace = 1200;
            pauseInverval = 40;
            break;
    }
}

function getTocke() {
    if (localStorage.tocke != null) {
        tocke = localStorage.tocke;
    } else {
        localStorage.tocke = 0;
    }
    $("#tocke").html(tocke);
}

var pavza = 0;

/*
 $(document).keydown(function (e) {
 if (e.keyCode == 87 || e.keyCode == 83 || e.keyCode == 65 || e.keyCode == 68) {
 if (soundManager.getSoundById('sikanje').playState == 1) {
 soundManager.resume('sikanje');
 }
 else {
 if (soundManager.getSoundById('sikanje').playState == 0) {
 soundManager.start('sikanje');
 }
 }
 }
 }
 );
 $(document).keyup(function (e) {
 if (e.keyCode == 87 || e.keyCode == 83 || e.keyCode == 65 || e.keyCode == 68) {
 if (soundManager.getSoundById('sikanje').playState == 1) {
 soundManager.pause('sikanje');
 pavza == 1;
 }
 else {
 pavza == 0;
 }
 }
 }
 );*/


soundManager.setup({
        url: '',
        debugMode: false,
        onfinish: function () {
        },
        onready: function () {
            var sik = soundManager.createSound({
                    id: 'sikanje',
                    url: './sounds/sikanje.mp3'
                }
            );
            var dingdong = soundManager.createSound({
                    id: 'ding',
                    url: './sounds/ding.mp3'
                }
            );
            var jabuk = soundManager.createSound({
                    id: 'jabuk',
                    url: './sounds/jabolk.mp3'
                }
            );
        }
    }
);

function konecCasa() {
    cancelAnimationFrame(id_render);
    gameOver();
}

//funkcija za čas
var pavzaCas = false;
var cas = 60 + 1;
var stevec = setInterval(timer, 1000);
//1000 will  run it every 1 second

function timer() {
    if(!pavzaCas){
        cas--;
        if (cas < 0) {
            clearInterval(stevec);
            return;
        }
        if (cas == 0) {
            $('#cas').css('color', 'red');
            soundManager.start('ding');
            konecCasa();
        }
        else {
            $('#cas').css('color', 'white');
        }
        $('#cas').text(cas);
    }

}

$("#shrani").click(function (){
    var neklevel=$("#levelSlider").val();
    localStorage.level=neklevel;
    localStorage.zoge=$("#zogeSlider").val();
    localStorage.kace=$("#kaceSlider").val();
    localStorage.zivljenja = 3;
    location.reload();
});


$(document).ready(function () {
        if(localStorage.level!=null){
            $('#levelSlider').val(localStorage.level);
        }
        if(localStorage.tezavnost!=null){
            $('#tezavnostSlider').val(localStorage.tezavnost);
        }

        if(localStorage.zoge!=null){
            $("#zogeSlider").val(localStorage.zoge);
        }
        if(localStorage.kace!=null){
            $("#kaceSlider").val(localStorage.kace);
        }
        if(localStorage.zvok!=null){
            if(localStorage.zvok=='off'){
                soundManager.mute();
            }else{
                soundManager.unmute();
            }
        }
        $("#nastavitveImg").click(function () {
                $('#top-ten').css('opacity', '0.0');
                $("#nastavitve").toggle(function () {
                        if ($("#nastavitve").is(":hidden")) {
                            $('#nastavitveImg').css('opacity', '1.0');
                            $('#top-ten').css('opacity', '1.0');
                        }
                        else {
                            $('#nastavitveImg').css('opacity', '0.8');
                        }
                    }
                );
            }
        );
    }
);

function getZivljenja() {
    if (localStorage.zivljenja != null) {
        zivljenja = localStorage.zivljenja;
    } else {
        localStorage.zivljenja = 3;
    }
    $("#zivljenja div").html(zivljenja);
}

function getLevel() {
    if (localStorage.level != null) {
        currentLevel = localStorage.level;
    } else {
        localStorage.level = 1;
    }
    $("#level").html(currentLevel);
}

function loadLevel() {
    var loader = new THREE.OBJLoader();
    loader.load("level/level" + currentLevel + ".obj", function (object) {
        var material1 = "";
        object.traverse(function (child) {
            if (child instanceof THREE.Mesh) {
                child.material.map = textureZid;
                material1 = child.material;
            }
        });
        object.children[0].geometry.computeFaceNormals();
        var geometry = object.children[0].geometry;
        var tempmesh = new THREE.Mesh(geometry, material1);
        tempmesh.position.x = 0;
        tempmesh.position.y = 300;
        tempmesh.position.z = 0;
        tempmesh.name = "telo";
        tempmesh.scale = new THREE.Vector3(150, 150, 150);
        stena = tempmesh;
        //collidableMeshList.push(tempmesh);

        // model = object;
        scene.add(tempmesh);

        //scene.add(object);


    });
}

function loadTexture(pot) {
    var loader = new THREE.ImageLoader();
    var texture = new THREE.Texture();
    loader.load(pot, function (image) {
        texture.image = image;
        texture.needsUpdate = true;
    });
    return texture;
}

function loadApple() {
    var loader = new THREE.OBJLoader();
    loader.load("apple/apple.obj", function (object) {
        var material1 = "";
        object.traverse(function (child) {
            if (child instanceof THREE.Mesh) {
                child.material.map = textureRdeca;
                material1 = child.material;
            }
        });
        object.children[0].geometry.computeFaceNormals();
        var geometry = object.children[0].geometry;
        var tempMesh = new THREE.Mesh(geometry, material1);
        jabolkoMesh = tempMesh;
    });
}

function loadApples() {
    var loader = new THREE.OBJLoader();
    loader.load("apple/apple.obj", function (object) {
        var geometry = object.children[0].geometry;
        for (var i = 0; i < 10; i++) {
            var material = new THREE.MeshLambertMaterial({color: '#dfc83e'});
            var tempMesh = new THREE.Mesh(geometry, material);
            tempMesh.position.x = (Math.random() * (2472 + 1588)) - 2472;
            tempMesh.position.y = 0;
            tempMesh.position.z = (Math.random() * (2844 + 1107)) - 1107;
            tempMesh.name = i + ":rdece:1";
            tempMesh.material.map = textureRdeca;
            tempMesh.scale = new THREE.Vector3(1, 1, 1);
            collidableMeshList.push(tempMesh);
            jabolka.push(tempMesh);
            scene.add(tempMesh);
        }
    });
}

function collisionApples(mesh) {
    var originPoint = mesh.position.clone();
    //return true;
    for (var vertexIndex = 0; vertexIndex < mesh.geometry.vertices.length; vertexIndex++) {
        var localVertex = mesh.geometry.vertices[vertexIndex].clone();
        var globalVertex = localVertex.applyMatrix4(mesh.matrix);
        var directionVector = globalVertex.sub(mesh.position);

        var ray = new THREE.Raycaster(originPoint, directionVector.clone().normalize());
        var collisionResults = ray.intersectObjects(collidableMeshList, true);
        if (collisionResults.length > 0) {
            console.log("pride do sekanja?");
            return false;
        }
    }
    return true;
}

function odpriVrata() {
    var position = 150;
    var interval = setInterval(function () {
        if (position < 0) {
            clearInterval(interval);
        } else {
            levaVrata.position.x -= 0.5;
            position = position - 0.5;
        }
    }, 5);

    var position2 = 0;
    var interval = setInterval(function () {
        if (position2 > 150) {
            clearInterval(interval);
        } else {
            desnaVrata.position.x += 0.5;
            position2 = position2 + 0.5;
        }
    }, 5);
}

function makeScene(angle, width, height, near, far) {
    scene = new THREE.Scene();
    var SCREEN_WIDTH = width, SCREEN_HEIGHT = height;
    var VIEW_ANGLE = angle, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = near, FAR = far;
    camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);
    scene.add(camera);
    camera.lookAt(scene.position);

    // orthographic cameras
    topCamera = new THREE.OrthographicCamera(
        window.innerWidth * -2,		// Left
        window.innerWidth * 4.4,		// Right
        window.innerHeight * 2,		// Top
        window.innerHeight * -4.4,	// Bottom
        -5000,            			// Near
        10000);           			// Far -- enough to see the skybox
    topCamera.up = new THREE.Vector3(0, 0, -1);
    topCamera.lookAt(new THREE.Vector3(0, -1, 0));
    scene.add(topCamera);
}

function makeBall() {
		for(var i=0;i<st_zog*1;i++)
		{
			var geometry = new THREE.SphereGeometry(25, 25, 25);
			var material = new THREE.MeshLambertMaterial({color: 0x00ffff});
			var zoga=new THREE.Mesh(geometry, material);
            zoga.position.x = (Math.random() * (2472 + 1588)) - 2472;
            zoga.position.y = 25.1;
            zoga.position.z = (Math.random() * (2844 + 1107)) - 1107;

            zoge.push(zoga);
			scene.add(zoga);

			var geometry1 = new THREE.CubeGeometry(50, 50, 50);
			var material1 = new THREE.MeshLambertMaterial({color: 0x00ffff, transparent: true, opacity: 0.0});
			var zogaBox = new THREE.Mesh(geometry1, material1);
            zogaBox.position.x = zoga.position.x;
            zogaBox.position.y = 25.1;
            zogaBox.position.z = zoga.position.z;
			zogeBox.push(zogaBox);
			scene.add(zogaBox);
		}
}

function initLights() {
    //lights
    var ambient = new THREE.AmbientLight(0x333333);
    scene.add(ambient);

    var pointLight = new THREE.PointLight(0xffffff, 1.5);
    pointLight.position.set(0, 1000, 1500);
    scene.add(pointLight);
    var lightTarget = new THREE.Object3D();
    lightTarget.position.set(0, 25, 1500);
    scene.add(lightTarget);
    pointLight.target = lightTarget;
    /*
     var pointLight1 = new THREE.PointLight(0xffffff);
     pointLight1.intensity =.7;
     pointLight1.position.set(34-4000, 1000, 0);
     scene.add(pointLight1);
     var lightTarget1 = new THREE.Object3D();
     lightTarget1.position.set(34-4000, 0, 0);
     scene.add(lightTarget1);
     pointLight1.target = lightTarget1;

     var pointLight2 = new THREE.PointLight(0xffffff);
     pointLight2.intensity = .7;
     pointLight2.position.set(34+4000, 1000, 0);
     scene.add(pointLight2);
     var lightTarget2 = new THREE.Object3D();
     lightTarget2.position.set(34+4000, 0, 0);
     scene.add(lightTarget2);
     pointLight2.target = lightTarget2;*/
}

function initTextures() {
    textureRdeca = loadTexture('apple/appleD.jpg');
    textureZlata = loadTexture('apple/appleZ.jpg');
    textureZid = loadTexture('level/zid.jpg');
    textureVrata = loadTexture('vrata/vrata.jpg')
}

function loadOBJMTLObjects() {
    var loader = new THREE.OBJMTLLoader();
    loader.load('kaca/snake.obj', 'kaca/snake.mtl', function (object) {
        object.position.x = 0;
        object.position.y = 30;
        object.position.z = 0;
        object.scale = new THREE.Vector3(2, 2, 2);
        //object.rotation.y = de2ra(90);
        scene.add(object);
        snake.setGlava(object);
        asycDone();

    });

    loader.load('level/trata.obj', 'level/trata.mtl', function (object) {
        object.position.x = 0;
        object.position.y = 400;
        object.position.z = 0;
        object.scale = new THREE.Vector3(150, 150, 150);
        scene.add(object);
    });
}

function de2ra(degree) {
    return degree * (Math.PI / 180);
}

function loadSnake() {
    snakeObject = snake.getObject();
    snakeObject.position.set(-270, 25.1, -1100);
    snakeObject.rotation.y = de2ra(-90);
    scene.add(snakeObject);

    snakeHead = snake.getGlava();
    snakeHead.position.set(-270, 25.1, -1100);
	snakeHead.rotation.y = de2ra(-90);
    scene.add(snakeHead);

    ostaleKace();

    scene.add(stena);
    stena.name = "telo";
    collidableMeshList.push(stena);
    //loadApples();

    scene.add(levaVrata);
    levaVrata.name = "telo";
    collidableMeshList.push(levaVrata);

    scene.add(desnaVrata);
    desnaVrata.name = "telo";
    collidableMeshList.push(desnaVrata);
}

function loadGates() {
    var loader = new THREE.OBJLoader();
    loader.load("vrata/vrata_desna.obj", function (object) {
        var material1 = "";
        object.traverse(function (child) {
            if (child instanceof THREE.Mesh) {
                child.material.map = textureVrata;
                material1 = child.material;
            }
        });
        object.children[0].geometry.computeFaceNormals();
        var geometry = object.children[0].geometry;
        var tempMesh = new THREE.Mesh(geometry, material1);
        tempMesh.position.x = 0;
        tempMesh.position.y = 100;
        tempMesh.position.z = -20;
        tempMesh.name = "telo";
        tempMesh.scale = new THREE.Vector3(150, 150, 150);
        desnaVrata = tempMesh;
    });

    loader.load("vrata/vrata_leva.obj", function (object) {
        var material1 = "";
        object.traverse(function (child) {
            if (child instanceof THREE.Mesh) {
                child.material.map = textureVrata;
                material1 = child.material;
            }
        });
        object.children[0].geometry.computeFaceNormals();
        var geometry = object.children[0].geometry;
        var tempMesh = new THREE.Mesh(geometry, material1);
        tempMesh.position.x = 0;
        tempMesh.position.y = 100;
        tempMesh.position.z = -20;
        tempMesh.name = "telo";
        tempMesh.scale = new THREE.Vector3(150, 150, 150);
        levaVrata = tempMesh;
    });
}

function changeTextureInterval() {
    setInterval(function () {
        var procent = Math.floor(Math.random() * 7);
        if (procent == 5) {
            var jabolkaIndex = Math.floor(Math.random() * 10);
            var tempMesh = jabolka[jabolkaIndex];
            tempMesh.material.map = textureZlata;
            var prejsnoIme = tempMesh.name.split(":");
            tempMesh.name = prejsnoIme[0] + ":zlato"+":"+prejsnoIme[2];
        }
    }, 1000);
}

function init() {
    makeScene(45, window.innerWidth, window.innerHeight - 230, .1, 20000);
    snake = new Snake();
    var teloKace = snake.getTelo();
    for (var i = 0; i < teloKace.length; i++) {
        scene.add(teloKace[i]);
    }
    for (var i = 0; i < kace; i++) {
        snakeOther1.push(new Snake());
    }
    makeBall();
    initLights();
    initTextures();
    loadGates();
    loadLevel();
    loadOBJMTLObjects();

    loadApples();
    changeTextureInterval();
    //naredimo renderer
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(WIDTH, HEIGHT);
    renderer.setClearColorHex(0xffffff, 1);
    renderer.autoClear = false;
    //canvas pripnemo v body
    document.body.appendChild(renderer.domElement);

    this.colorRed = THREE.ImageUtils.loadTexture("images/left.gif");
    this.colorGreen = THREE.ImageUtils.loadTexture("images/SquareGreen.png");
    this.colorBlue = THREE.ImageUtils.loadTexture("images/right.gif");

    // VIDEO SET UP

    // these changes are permanent
    videoContext.translate(320, 0);
    videoContext.scale(-1, 1);

    // background color if no video present
    videoContext.fillStyle = '#005337';
    videoContext.fillRect(0, 0, videoCanvas.width, videoCanvas.height);

    buttons = [];

    var button1 = new Image();
    button1.src = "images/left.gif";
    var buttonData1 = { name: "red", image: button1, x: 0, y: 0, w: 50, h: 230 };
    buttons.push(buttonData1);

    var button2 = new Image();
    button2.src = "images/up.gif";
    var buttonData2 = { name: "green", image: button2, x: 140, y: 0, w: 60, h: 60 };
    buttons.push(buttonData2);

    var button3 = new Image();
    button3.src = "images/right.gif";
    var buttonData3 = { name: "blue", image: button3, x:  280, y: 0, w: 50, h: 230};
    buttons.push(buttonData3);
}

function moveBodyMySnake() {
    var telo = snake.getTelo();
    var prevX = snakeObject.position.x;
    var prevY = snakeObject.position.z;
    var prevXtemp = telo[0].position.x;
    var prevYtemp = telo[0].position.z;
    telo[0].position.x = prevX;
    telo[0].position.z = prevY;
    for (var i = 1; i < telo.length; i++) {
        var t1 = telo[i].position.x;
        var t2 = telo[i].position.z;
        telo[i].position.x = prevXtemp;
        telo[i].position.z = prevYtemp;
        prevXtemp = t1;
        prevYtemp = t2;
    }
}

function moveBodyOtherSnakes() {
    for(var j=0;j<kace;j++){
        var telo = snakeOther1[j].getTelo();
        var prevX = objektGlave[j].position.x;
        var prevY = objektGlave[j].position.z;
        var prevXtemp = telo[0].position.x;
        var prevYtemp = telo[0].position.z;
        telo[0].position.x = prevX;
        telo[0].position.z = prevY;
        for (var i = 1; i < telo.length; i++) {
            var t1 = telo[i].position.x;
            var t2 = telo[i].position.z;
            telo[i].position.x = prevXtemp;
            telo[i].position.z = prevYtemp;
            prevXtemp = t1;
            prevYtemp = t2;
        }
    }
}

function moveBody() {
    setInterval(function () {
        moveBodyMySnake();
        moveBodyOtherSnakes();
    }, pauseInverval);
}

function asycDone() {
    loadSnake();
    animate();
    moveBody();
}

function addSnakeBodyOtherSnake(i){
    var index = snakeOther1[i].addTelo();
    var teloTemp =  snakeOther1[i].getTeloByIndex(index);
    teloTemp.name = "telo";
    collidableMeshList.push(teloTemp);
    scene.add(teloTemp);
}

function addSnakeBody() {
    var index = snake.addTelo();
    var teloTemp = snake.getTeloByIndex(index);
    teloTemp.name = "telo";
    collidableMeshList.push(teloTemp);
    scene.add(teloTemp);
}

function animate() {
    id_render = requestAnimationFrame(animate);
    render();
    update();
}

function moveBall() {
	for(var i=0;i<st_zog*1;i++)
	{
		zoge[i].position.x += hitrostX[i];
		zoge[i].position.z += hitrostY[i];
		zogeBox[i].position.x += hitrostX[i];
		zogeBox[i].position.z += hitrostY[i];
	}
}

function calculateDistance(x1, y1, x2, y2) {
    var xd = x2 - x1;
    var yd = y2 - y1;
    return Math.sqrt(xd * xd + yd * yd);
}

var ciljTargetiran = new Array(false,false,false);
var deltaX = 0;
var deltaY = 0;
var minimum = 0;

function moveSnake() {
    var delta = clock.getDelta(); // seconds.
    var moveDistance = hitrostKace * delta; // 200 pixels per second
    rotateAngle = Math.PI * delta;   // pi/2 radians (90 degrees) per second
    snakeObject.translateZ(-moveDistance);
    snakeHead.translateZ(-moveDistance);

    for(var j = 0; j < kace;j++){
        if (!ciljTargetiran[j]) {
            var ArrayDistance = [];
            var minindex = 0;
            for (var i = 0; i < 10; i++) {
                ArrayDistance.push(calculateDistance(objektGlave[j].position.x, objektGlave[j].position.z, jabolka[i].position.x, jabolka[i].position.z));
            }

            //console.log(ArrayDistance);
            minimum = 50000;
            for(var i=1;i<ArrayDistance.length;i++){
                var ime = jabolka[i].name.split(":");
                if(ArrayDistance[i] < minimum && ime[2] == "1"){
                    minimum = ArrayDistance[i];
                    minindex = i;
                }
            }
            var tempx = jabolka[minindex].position.x;
            var tempy = jabolka[minindex].position.z;

            deltaX = (tempx - objektGlave[j].position.x) / minimum * 4;
            deltaY = (tempy - objektGlave[j].position.z) / minimum * 4;
            ciljTargetiran[j] = true;
        }

        objektGlave[j].translateZ(-deltaY);
        glava[j].translateZ(-deltaY);

        objektGlave[j].translateX(-deltaX);
        glava[j].translateX(-deltaX);
    }

    var rotation_matrix = new THREE.Matrix4().identity();
    if (keyboard.pressed("A")) {
        snakeObject.rotateOnAxis(new THREE.Vector3(0, 1, 0), rotateAngle);
        snakeHead.rotateOnAxis(new THREE.Vector3(0, 1, 0), rotateAngle);
    }

    if (keyboard.pressed("D")) {
        snakeObject.rotateOnAxis(new THREE.Vector3(0, 1, 0), -rotateAngle);
        snakeHead.rotateOnAxis(new THREE.Vector3(0, 1, 0), -rotateAngle);
    }

    if (left) {
        snakeObject.rotateOnAxis(new THREE.Vector3(0, 1, 0), rotateAngle);
        snakeHead.rotateOnAxis(new THREE.Vector3(0, 1, 0), rotateAngle);
    }

    if (right) {
        snakeObject.rotateOnAxis(new THREE.Vector3(0, 1, 0), -rotateAngle);
        snakeHead.rotateOnAxis(new THREE.Vector3(0, 1, 0), -rotateAngle);
    }

    var relativeCameraOffset = new THREE.Vector3(0, 200, 500);
    var cameraOffset = relativeCameraOffset.applyMatrix4(snakeObject.matrixWorld);
    camera.position.x = cameraOffset.x;
    camera.position.y = cameraOffset.y;
    camera.position.z = cameraOffset.z;
    camera.lookAt(snakeObject.position);
}

function collisionSnakeOther1() {
    for(var j = 0; j < kace;j++){
        var originPoint = objektGlave[j].position.clone();
        for (var vertexIndex = 0; vertexIndex < objektGlave[j].geometry.vertices.length; vertexIndex++) {
            var localVertex = objektGlave[j].geometry.vertices[vertexIndex].clone();
            var globalVertex = localVertex.applyMatrix4(objektGlave[j].matrix);
            var directionVector = globalVertex.sub(objektGlave[j].position);

            var ray = new THREE.Raycaster(originPoint, directionVector.clone().normalize());
            var collisionResults = ray.intersectObjects(collidableMeshList, true);
            if (collisionResults.length > 0 && collisionResults[0].distance < directionVector.length()) {
                var nameOfObject = collisionResults[0].object.name;
                if (nameOfObject != "telo") {
                    //jabolka.splice();
                    var geometry = new THREE.SphereGeometry(15, 15, 25);
                    var material = new THREE.MeshLambertMaterial({color: '#dfc83e'});
                    var tempX = new THREE.Mesh(geometry, material);
                    tempX.position.x = 20000;
                    tempX.position.z = 0;
                    tempX.position.y = -5000;
                    var arrayName = nameOfObject.split(":");
                    collidableMeshList[parseInt(arrayName[0])] = tempX;
                    jabolka[parseInt(arrayName[0])].name = arrayName[0]+":"+arrayName[1]+":0";
                    //collidableMeshList.splice(parseInt(t),1);
                    //console.log(collidableMeshList);
                    //collidableMeshList[collidableMeshList.length-1].name = t;
                    scene.remove(collisionResults[0].object);
                    for (var i = 0; i < steviloTeles; i++)
                        addSnakeBodyOtherSnake(j);
                    telesa++;
                    ciljTargetiran = false;
                    odstejTocke(arrayName[1]);
                    //console.log("stevilo teles: "+telesa);
                    break;
                } else {
                    //gameOver();
                    break;
                }
            }
        }
    }
}

function collisionSnake() {
    var originPoint = snakeObject.position.clone();
    for (var vertexIndex = 0; vertexIndex < snakeObject.geometry.vertices.length; vertexIndex++) {
        var localVertex = snakeObject.geometry.vertices[vertexIndex].clone();
        var globalVertex = localVertex.applyMatrix4(snakeObject.matrix);
        var directionVector = globalVertex.sub(snakeObject.position);

        var ray = new THREE.Raycaster(originPoint, directionVector.clone().normalize());
        var collisionResults = ray.intersectObjects(collidableMeshList, true);
        if (collisionResults.length > 0 && collisionResults[0].distance < directionVector.length()) {
            var nameOfObject = collisionResults[0].object.name;
            if (nameOfObject != "telo") {
                //jabolka.splice();
                var geometry = new THREE.SphereGeometry(15, 15, 25);
                var material = new THREE.MeshLambertMaterial({color: '#dfc83e'});
                var tempX = new THREE.Mesh(geometry, material);
                tempX.position.x = 20000;
                tempX.position.z = 0;
                tempX.position.y = -5000;
                var arrayName = nameOfObject.split(":");
                collidableMeshList[parseInt(arrayName[0])] = tempX;
                jabolka[parseInt(arrayName[0])].name = arrayName[0]+":"+arrayName[1]+":0";
                //collidableMeshList.splice(parseInt(t),1);
                //console.log(collidableMeshList);
                //collidableMeshList[collidableMeshList.length-1].name = t;
                scene.remove(collisionResults[0].object);
                for (var i = 0; i < steviloTeles; i++)
                    addSnakeBody();
                telesa++;
                ciljTargetiran = false;
                pristejTocke(arrayName[1]);
                //console.log("stevilo teles: "+telesa);
                break;
            } else {
                gameOver();
                break;
            }
        }
    }
}

function gameOver() {
    cancelAnimationFrame(id_render);
    zivljenja--;
    localStorage.zivljenja = zivljenja;
    localStorage.tocke = tocke;
    if (zivljenja <= 0) {
        cancelAnimationFrame(id_render);
        preveriTocke(tocke);
       // newGame();
        return;
    } else {
        location.reload();
    }

}

function checkNextLevel() {
    if (telesa == 10) {
        odpriVrata();
        telesa = 0;
        odprtaVrata = true;
    }
}

function nextLevel() {
    currentLevel++;
    localStorage.level = currentLevel;
    localStorage.tocke = tocke;
    cancelAnimationFrame(id_render);
    location.reload();
}

function moveOtherSnakes() {

}

function update() {
    //console.log("x: "+snakeObject.position.x+", z: "+snakeObject.position.z);
    checkNextLevel();
	checkCollisionBall();
	moveBall();
    moveOtherSnakes();
    moveSnake();
    collisionSnake();
    collisionSnakeOther1();

    if (odprtaVrata) {
        // console.log(snakeObject.position.z);
        if (snakeObject.position.z < -1300) {
			tocke=tocke*1+currentLevel*1+kace*2+st_zog*1;
			$("#tocke").html(tocke);
            nextLevel()
        }
    }

    blend();
    checkAreas();
}

function blend() {
    var width = videoCanvas.width;
    var height = videoCanvas.height;
    // get current webcam image data
    var sourceData = videoContext.getImageData(0, 0, width, height);
    // create an image if the previous image doesn�t exist
    if (!lastImageData) lastImageData = videoContext.getImageData(0, 0, width, height);
    // create a ImageData instance to receive the blended result
    var blendedData = videoContext.createImageData(width, height);
    // blend the 2 images
    differenceAccuracy(blendedData.data, sourceData.data, lastImageData.data);
    // draw the result in a canvas
    blendContext.putImageData(blendedData, 0, 0);
    // store the current webcam image
    lastImageData = sourceData;
}
function differenceAccuracy(target, data1, data2) {
    if (data1.length != data2.length) return null;
    var i = 0;
    while (i < (data1.length * 0.25)) {
        var average1 = (data1[4 * i] + data1[4 * i + 1] + data1[4 * i + 2]) / 3;
        var average2 = (data2[4 * i] + data2[4 * i + 1] + data2[4 * i + 2]) / 3;
        var diff = threshold(fastAbs(average1 - average2));
        target[4 * i] = diff;
        target[4 * i + 1] = diff;
        target[4 * i + 2] = diff;
        target[4 * i + 3] = 0xFF;
        ++i;
    }
}
function fastAbs(value) {
    return (value ^ (value >> 31)) - (value >> 31);
}
function threshold(value) {
    return (value > 0x15) ? 0xFF : 0;
}

// check if white region from blend overlaps area of interest (e.g. triggers)
function checkAreas() {
    for (var b = 0; b < buttons.length; b++) {
        // get the pixels in a note area from the blended image
        var blendedData = blendContext.getImageData(buttons[b].x, buttons[b].y, buttons[b].w, buttons[b].h);

        // calculate the average lightness of the blended data
        var i = 0;
        var sum = 0;
        var countPixels = blendedData.data.length * 0.25;
        while (i < countPixels) {
            sum += (blendedData.data[i * 4] + blendedData.data[i * 4 + 1] + blendedData.data[i * 4 + 2]);
            ++i;
        }
        // calculate an average between of the color values of the note area [0-255]
        var average = Math.round(sum / (3 * countPixels));
        if (average > 5) // more than 20% movement detected
        {
            console.log("Button " + buttons[b].name + " triggered."); // do stuff
            if (buttons[b].name == "red")
                motionLeft();
            if (buttons[b].name == "green")
                motionForward();
            if (buttons[b].name == "blue")
                motionRight();
            // messageArea.innerHTML = "Button " + buttons[b].name + " triggered.";
        }
        // console.log("Button " + b + " average " + average);
    }
}

var right = false;
var left = false;

function motionLeft() {
    right = false;
    left = true;
}

function motionRight() {
    left = false;
    right = true;
}

function motionForward() {
    right = false;
    left = false;
}

function odstejTocke(tipJabolke){
    console.log("kuga");
	if (tipJabolke == "rdece") {
		if((parseInt(tocke)-2)>=0)
			tocke = tocke * 1 - 2;
    }
    else if (tipJabolke == "zlato") {
		if((parseInt(tocke)-5)>=0)
			tocke = tocke * 1 - 5;
    }
	$("#tocke").html(tocke);
}

function pristejTocke(tipJabolke){
	getTezavnost();
    switch (tezavnost) {
        case '0':
            if (tipJabolke == "rdece") {
                tocke = tocke * 1 + 1;
            }
            else if (tipJabolke == "zlato") {
                tocke = tocke * 1 + 5+kace*1; //+1 za vsako računalniško kačo
            }
            break;

        case '1':
            if (tipJabolke == "rdece") {
                tocke = tocke * 1 + 2;
            }
            else if (tipJabolke == "zlato") {
                tocke = tocke * 1 + 7+kace*1; //+1 za vsako računalniško kačo
            }
            break;

        case '2':
            if (tipJabolke == "rdece") {
                tocke = tocke * 1 + 3;
            }
            else if (tipJabolke == "zlato") {
                tocke = tocke * 1 + 9+kace*1; //+1 za vsako računalniško kačo
            }
            break;

        case '3':
            if (tipJabolke == "rdece") {
                tocke = tocke * 1 + 4;
            }
            else if (tipJabolke == "zlato") {
                tocke = tocke * 1 + 11+kace*1; //+1 za vsako računalniško kačo
            }
            break;
    }
	if((parseInt(tocke)%50)==0 &&parseInt(zivljenja)<7)
	{
		zivljenja++;
		$("#zivljenja").append("<img src='heart.png' width='30' height='30'/>");
		localStorage.zivljenja = zivljenja;
	}
	$("#tocke").html(tocke);
}

function changeDirection(index) {
    hitrostX[index] = -hitrostX[index];
    // hitrostY = -hitrostY;
}

function newGame() {
    localStorage.level = 1;
    localStorage.tocke = 0;
    localStorage.zivljenja = 3;
    location.reload();
}

function checkCollisionBall() {
	for(var i=0; i<st_zog;i++)
	{
        if(zogeBox[i].position.z < -1200){
            hitrostY[i] = -hitrostY[i];
        } else if(zogeBox[i].position.z > 3000){
            hitrostY[i] = -hitrostY[i];
        }
		var originPoint = zogeBox[i].position.clone();
		for (var vertexIndex = 0; vertexIndex < zogeBox[i].geometry.vertices.length; vertexIndex++) {
			var localVertex = zogeBox[i].geometry.vertices[vertexIndex].clone();
			var globalVertex = localVertex.applyMatrix4(zogeBox[i].matrix);
			var directionVector = globalVertex.sub(zogeBox[i].position);

			var ray = new THREE.Raycaster(originPoint, directionVector.clone().normalize());
			var collisionResults = ray.intersectObjects(collidableMeshList, true);
			if (collisionResults.length > 0 && collisionResults[0].distance < directionVector.length()) {
				changeDirection(i);
				return;
			}
		}
	}
}

function dodajNaTopTen(){
    var ime = $(".ime").val();
    var priimek = $(".priimek").val();
    var email = $(".email").val();

    var polje = new Array();
    polje.push(ime);
    polje.push(priimek);
    polje.push(email);
    polje.push(tocke);

    var p = new Array();

    $.ajax({
        type: "POST",
        url: "rezultati.php",
        data: {
            polje: polje
        }
    }).done(function (data) {
            var p = jQuery.parseJSON(data);
            var tabela = "";
            for (var i = 0; i < p.rezultat.length; i++) {
                tabela += "<tr>" +
                    "<td>" + p.rezultat[i].ime + " " + p.rezultat[i].priimek + "</td>" +
                    "<td>" + p.rezultat[i].tocke + "</td>" +
                    "</tr>";
            }

            $("<div id='naslov'>TOP TEN</div><div id='tabela'><table>" + tabela + "</table><p onclick='newGame()' id='nova-igra'>Nova igra</p></div>").insertAfter("#prijava");
            $("#prijava").remove();

        });
}

function preveriTocke(tocka) {
    var prijava = "<div id='top-ten'>" +
        "<div id='zapri-tt' onclick='zapriTopTen()'>X</div>" +
        "<table id='prijava'>" +
        "<tr>" +
        "<td><strong>Ime:</strong></td>" +
        "<td><input type='text' name='ime' class='ime'/></td>" +
        "</tr>" +
        "<tr>" +
        "<td><strong>Priimek:</strong></td>" +
        "<td><input type='text' name='priimek' class='priimek'/></td>" +
        "</tr>" +
        "<tr>" +
        "<td><strong>Elektronski naslov:</strong></td>" +
        "<td><input type='email' name='email' class='email'/></td>" +
        "</tr>" +
        "<tr>" +
        "<td></td>" +
        "<td><input type='button' value='Vpiši se!' onclick='dodajNaTopTen()' class='vpisi-se'/></td>" +
        "</tr>" +
        "</table>" +
        "</div>";

    var t = new Array();
    t.push(tocka);

    $.ajax({
        type: "POST",
        url: "rezultati.php",
        data: {
            trenutneTocke: t
        }
    }).done(function (data) {
            var p = jQuery.parseJSON(data);
            if(p.bool == true){
                $(prijava).insertAfter("#nastavitve");
                dodajNaLestvico = true;
            }
            else{
                var p = jQuery.parseJSON(data);
                var tabela = "";
                for (var i = 0; i < p.rezultat.length; i++) {
                    tabela += "<tr>" +
                        "<td>" + p.rezultat[i].ime + " " + p.rezultat[i].priimek + "</td>" +
                        "<td>" + p.rezultat[i].tocke + "</td>" +
                        "</tr>";
                }
                $("<div id='top-ten'><div id='zapri-tt' onclick='zapriTopTen()'>X</div><div id='naslov'>TOP TEN</div><div id='tabela'><table>" + tabela + "</table><p onclick='newGame()' id='nova-igra'>Nova igra</p></div></div>").insertAfter("#nastavitve");
            }
        });
}

function zapriTopTen(){
    if($("#top-ten").length > 0){
        $("#top-ten").remove();
    }
}

function render() {
    // renderer.render(scene, camera);
    var SCREEN_WIDTH = WIDTH, SCREEN_HEIGHT = HEIGHT;

    renderer.setViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    renderer.clear();

    renderer.setViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    renderer.render(scene, camera);

    //mala kamerca
    renderer.setViewport(SCREEN_WIDTH - 300, 0, 600, 200);
    renderer.render(scene, topCamera);

    if (video.readyState === video.HAVE_ENOUGH_DATA) {
        // mirror video
        videoContext.drawImage(video, 0, 0, videoCanvas.width, videoCanvas.height);
        for (var i = 0; i < buttons.length; i++)
            layer2Context.drawImage(buttons[i].image, buttons[i].x, buttons[i].y, buttons[i].w, buttons[i].h);
    }
}

var objektGlave = new Array();
var glava = new Array();

function ostaleKace() {
    for(var i = 0; i < kace;i++){
        objektGlave.push(snakeObject.clone());
        var x = (Math.random() * (2472 + 1588)) - 2472;
        var z = (Math.random() * (2844 + 1107)) - 1107;
        objektGlave[i].position.set(x, 25.1, z);
        objektGlave[i].name = "telo";
        collidableMeshList.push(objektGlave[i]);
        scene.add(objektGlave[i]);

        glava.push(snakeHead.clone());
        glava[i].position.set(100, 25.1, -1200);
        //scene.add(glava);

        var teloKace = snakeOther1[i].getTelo();
        for (var i1 = 0; i1 < teloKace.length; i1++) {
            scene.add(teloKace[i1]);
            teloKace[i1].name = "telo";
            collidableMeshList.push(teloKace[i1]);
        }
    }

}

function pause(){
    cancelAnimationFrame(id_render);
    pavzaCas = true;
}

function resume(){
    requestAnimationFrame(animate);
    pavzaCas = false;
}

var onKeyDown = function (event) {
    switch (event.keyCode) {
        case 13:
            console.log("x: "+snakeObject.position.x +", z: "+snakeObject.position.z);
            break;
        case 90:
            resume();
            break;
        case 78:
            newGame();
            break;
        case 80:
            pause();
            break;
    }
};

document.addEventListener('keydown', onKeyDown, false);
</script>
</body>
</html>