function Tockovanje(level){
	this.level = level;
	this.tocke = 0;
	this.cas = 100;
}
Tockovanje.prototype.casovnik = function(){
  	var interval = setInterval(function(){
	    if (this.cas == 0) {
	        clearInterval(interval);
	    } else {
		    this.cas--;
	    }
  	}, 1000);
};
Tockovanje.prototype.setCas = function(cas){
	this.cas = cas;
};
Tockovanje.prototype.getCas = function(){
	return this.cas;
};
Tockovanje.prototype.changeCas = function(){
	this.cas--;
};
Tockovanje.prototype.check = function(){
	return this.cas == 0 ? true : false;
};
Tockovanje.prototype.setTocke = function(tocke){
	this.tocke = tocke;
};
Tockovanje.prototype.getTocke = function(){
	return this.tocke;
};