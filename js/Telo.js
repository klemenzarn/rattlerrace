//razred telo
function Telo(x,y){
	this.x = x;
	this.y = y;
}
Telo.prototype.getX = function(){
	return this.x;
};
Telo.prototype.getY = function(){
	return this.y;
};
Telo.prototype.setX = function(x){
	this.x = x;
};
Telo.prototype.setY = function(y){
	this.y = y;
};
Telo.prototype.setCords = function(x,y){
	this.x = x;
	this.y = y;
};