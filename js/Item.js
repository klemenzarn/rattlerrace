function Item(name, x, y, tocke1, tocke2, tocke3, tocke4){
	this.name = name;
	this.x = x;
	this.y = y;
	this.tockeBegginer = tocke1;
	this.tockeInter = tocke2;
	this.tockeAdvanced = tocke3;
	this.tockeExpert = tocke4;
}
Item.prototype.getName = function(){ 
	return this.name
};
Item.prototype.setName = function(name){
	this.name = name;
}
Item.prototype.getX = function(){
	return this.x;
};
Item.prototype.getY = function(){
	return this.y;
};
Item.prototype.setX = function(x){
	this.x = x;
};
Item.prototype.setY = function(y){
	this.y = y;
};
Item.prototype.getTockeBegginer = function(){
	return this.tockeBegginer;
};
