//razred snake
function Snake() {
    this.dolzina = 10;
    var geometry = new THREE.CubeGeometry(40,40,40);
    this.glavica = null;
    //ustvarimo material zelene barve
    var material = new THREE.MeshLambertMaterial({color: 0xff0000,transparent: true, opacity:0.0});
    this.kocka = new THREE.Mesh(geometry, material);

    this.x = 0;
    this.y = 0;
    this.zivljenje = 3;
    this.telo = new Array();
    for (var i = 0; i < this.dolzina; i++) {
        var geometry = new THREE.SphereGeometry(15, 15, 25);
        var material = new THREE.MeshLambertMaterial({color: '#dfc83e'});
        var kocka = new THREE.Mesh(geometry, material);
        kocka.position.x = 0;
        kocka.position.z = -1500;
        kocka.position.y = 25.1;
        this.telo.push(kocka);
    }
}
Snake.prototype.getGlava = function(){
    return this.glavica;
};
Snake.prototype.setGlava = function(glava) {
    this.glavica = glava;
};
Snake.prototype.getTelo = function () {
    return this.telo;
}
Snake.prototype.getX = function () {
    return this.x;
};
Snake.prototype.getY = function () {
    return this.y;
};
Snake.prototype.setX = function (x) {
    this.x = x;
};
Snake.prototype.setY = function (y) {
    this.y = y;
};
Snake.prototype.getObject = function () {
    return this.kocka;
};
Snake.prototype.checkCollision = function (item) {
    if (this.x == item.getX() && this.y == item.getY()) {
        return true;
    }
    return false;
};

Snake.prototype.addTelo = function () {
    var kocka = this.telo[5].clone();
    this.telo.push(kocka);
    return this.telo.length-1;
};
Snake.prototype.getTeloByIndex = function (index) {
    return this.telo[index];
};
Snake.prototype.move = function (velocityX, velocityY) {
    var prevX = this.x;
    var prevY = this.y;
    /*
     *  if (velocityX == 1) {
     kocka.position.x += 1;
     } else if (velocityX == -1) {
     kocka.position.x -= 1;
     } else if (velocityY == -1) {
     kocka.position.z += 1;
     } else if (velocityY == 1) {
     kocka.position.z -= 1;
     }
     * */

    if (velocityX == 1) {
        this.x += 1;
    } else if (velocityX == -1) {
        this.x -= 1;
    } else if (velocityY == -1) {
        this.y += 1;
    } else if (velocityY == 1) {
        this.y -= 1;
    }

    this.kocka.position.x = this.x;
    this.kocka.position.z = this.y;

    /*for (var i = 0; i < this.telo.length; i++) {
     prevX = this.telo[i].getX();
     prevY = this.telo[i].getY();
     this.telo[i].setCords(prevX, prevY);
     }*/
}